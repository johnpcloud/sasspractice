// function searchStudent()
// {
	// var sName = document.getElementsByName("student-name")[0].value;
	// var sRoll = document.getElementsByName("student-roll")[0].value;
	// console.log(typeof(sName));
	// var sName=string(Name);
	// if(sName != '' && !isNaN(sRoll)){
		// console.log('1st Name: '+name+' and Roll: '+roll);
		// console.log(student);
		
		// /*	Searching the data*/
		// for (var r = 0; r < student.length; r++) {
			// console.log(student[r]);
			// console.log(sName);
			// console.log(sRoll);
			// if(student[r].name === sName && student[r].roll === sRoll){
				// console.log('Conditions matched');
			// }	
		// }
		
		// var result = student.filter(obj => {
		  // return obj.name === sName
		// })
		// console.log(result);
		
		// /*	Searching the data ends*/
	// }
	// else{
		// console.log('I am EMPTY!!!!');
	// }
// }

/*	calling the function on page load */
// setTimeout(function() { studentTable(); }, 100);
window.onload = function() {
  studentTable();
};
/*	calling the function on page load ends*/



/* Rendering table by the id of a div*/
function studentTable(){
var table = '<div class="student-data">';
// table = '';
table+="<table>";
table+="<thead>";
for (var r = 0; r < 1; r++) {
	table+="<tr>";
	var objKey = Object.keys(student[r]);
	//console.log(objKey[1]);
	for(var c=0; c<objKey.length; c++){
		//console.log(objKey[c]);
		var key=objKey[c];
		//console.log(key);
		table+='<th>'+key+'</th>';
	}
	table+='<th>Action</th>';
	table+="</tr>";
}
table+="</thead>";
table+="<tbody>";
	//var con = student.length;
	//console.log(con);
	var id=new Array();
for (var r = 0; r < student.length; r++) {
	table+="<tr>";
	var objKey = Object.keys(student[r]);
	//console.log(objKey.length);
	for(var c=0; c<objKey.length; c++){
		//console.log(objKey[c]);
		var key=objKey[c];
		//console.log(key);
		table+='<td >'+ student[r][key]+'</td>';
		// var rIndex = this.parentElement.rowIndex;
	}
	id[r]=r;
	//console.log(id[r]);
	table+='<td class="action"><button class="edit popupModal" onclick="editStudent('+id[r]+')"><i class="fas fa-edit"></i></i></button><button class="delete" onclick="deleteStudent('+id[r]+')"><i class="fas fa-trash-alt"></i></button></td>';
	
	table+="</tr>";
}
table+="</tbody>";
table+="<tfoot>";
table+="</tfoot>";
table+="</table>";
table+="</div>";
document.getElementById("student").innerHTML = table;
}
/* Rendering table by the id of a div*/



/* Attributes & Functions for modal	*/
var modal = document.querySelector(".modal");
var trigger = document.querySelector(".popupModal");
var closeButton = document.querySelector(".close-button");
window.addEventListener("click", windowOnClick);

function toggleModal() {
    modal.classList.toggle("show-modal");
}
function closeModal() {
    modal.classList.toggle("show-modal");
}
function windowOnClick(event) {
    if (event.target === modal) {
        toggleModal();
    }
}
/* Attributes & Functions for modal Ends	*/


/*	Add New Student	*/
function addStudent(){
	
	toggleModal();
	
	editModal='<div class="modal-content">';
	editModal+='<span class="close-button" onclick="closeModal()">×</span>';
	editModal+='<h2>Add Student Profile</h2>'
	editModal+=		'<div class="modal-data">';
	editModal+=			'<div class="data-items">';
	editModal+=				'<label for="new-student-name">Student Name: </label>';
	editModal+=				'<input class="input" type="text" name="new-student-name" value="" placeholder="Enter Student Name">';
	editModal+=			'</div>';
	editModal+=			'<div class="data-items">';
	editModal+=				'<label for="new-student-roll">Student Roll: </label>';
	editModal+=				'<input class="input" type="number" name="new-student-roll" value="" placeholder="Enter Student Roll">';
	editModal+=			'</div>';
	editModal+=			'<div class="data-items">';
	editModal+=				'<button class="btn-edit" onclick="storeStudent()">Save</button>';
	// editModal+=				'<button class="btn-delete" onclick="closeModal()">Cancle</button>';
	editModal+=			'</div>';
	editModal+=		'</div>';
	editModal+='</div>';
	
	document.querySelector(".modal").innerHTML = editModal;
}
function storeStudent() {
	
	// Catching the data via name element
    var sName = document.getElementsByName("new-student-name")[0].value;
	var sRoll = document.getElementsByName("new-student-roll")[0].value;
	//console.log(sName);
	//student[id].name = sName;
	//student[id].roll = sRoll;
	
	// Create new object with the data
	var newStudent = {
		name: sName,
		roll: parseInt(sRoll)
	}
	
	// Pushing the data to existing object
	student.push(newStudent);
	// console.log(student);
	
	// Viewing the table with updated student object
	studentTable();
	
	// Close the modal
	closeModal();
	
}
/*	Add New Student Ends	*/



/*edit information*/
function editStudent(id){
	//console.log(id);
	toggleModal();
	
	editModal='<div class="modal-content">';
	editModal+='<span class="close-button" onclick="closeModal()">×</span>';
	editModal+='<h2>Edit Student Profile</h2>'
	editModal+=		'<div class="modal-data">';
	editModal+=			'<div class="data-items">';
	editModal+=				'<label for="edit-student-name">Student Name: </label>';
	editModal+=				'<input class="input" type="text" name="edit-student-name" value="'+student[id].name+'">';
	editModal+=			'</div>';
	editModal+=			'<div class="data-items">';
	editModal+=				'<label for="edit-student-roll">Student Roll: </label>';
	editModal+=				'<input class="input" type="number" name="edit-student-roll" value="'+student[id].roll+'">';
	editModal+=			'</div>';
	editModal+=			'<div class="data-items">';
	editModal+=				'<button class="btn-edit" onclick="updateStudent('+id+')">Update</button>';
	// editModal+=				'<button class="btn-delete" onclick="closeModal()">Cancle</button>';
	editModal+=			'</div>';
	editModal+=		'</div>';
	editModal+='</div>';
	document.querySelector(".modal").innerHTML = editModal;
}
function updateStudent(id) {
	
	// Catching the data via name element
    var sName = document.getElementsByName("edit-student-name")[0].value;
	var sRoll = document.getElementsByName("edit-student-roll")[0].value;
	//console.log(sName);
	//student[id].name = sName;
	//student[id].roll = sRoll;
	
	//	update the data and assign the value
	Object.assign(student[id], {
		name: sName,
		roll: parseInt(sRoll)
		}
	)
	// console.log(student);
	
	// Viewing the table with updated Student object
	studentTable();
	
	// Close the modal
	closeModal();
	
}
/*edit information ends*/



/*Deleting information*/
function deleteStudent(id) {
	
	student.splice(id,1);
	studentTable();
	// console.log(id);
	// console.log(student);
	
	
}
/*Deleting information ends*/
